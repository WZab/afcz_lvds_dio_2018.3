#!/bin/bash
# You may need to adjust the paths to Vivado and Petalinux installations below:
VIVADO_DIR=/opt/Xlx/Viv2018.3/Vivado/2018.3
PETALINUX_DIR=/opt/petalinux/2018.3
set -e
# Run Vivado in a separate subshell
(
  cd hardware
  # In my setup setting the environment may generate errors due to uninstalled
  # old versions of Vivado. Therefore, I use "set +e" temporarily.
  set +e
  source ${VIVADO_DIR}/settings64.sh
  set -e
  ./build.sh
)
# Run Petalinux in a separate subshell
(
  cd software
  source ${PETALINUX_DIR}/settings.sh
  ./build.sh
)

