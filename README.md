# AFCZ-based test framework for 32 channel FMC DIO 32CH LVDS cards 

**This project is financed by [Creotech Instruments SA](http://creotech.pl) and supported from European Regional Development Fund in the framework of NCBR "IMPAKT" project**

**Please note that both Vivado and Petalinux projects are prepared for Vivado 2018.3**

This repository contain complete framework for testing of [FMC DIO 32CH LVDS](https://www.ohwr.org/project/fmc-dio-32chlvdsa) cards, connected to the [AFCZ](https://github.com/elhep/AFCZ) card.
The design consists of two main parts:
1. The Vivado project producing the PL design
1. The Petalinux project that generates the Linux image with software components.

# The old (well tested) method for building the design
To produce the working design you should clone the repository.
Then in the "hardware" subdirectory you should run (after setting the Vivado
environment):

    vivado -mode batch -source AFCZ_DIO.tcl
	
That should create the `AFCZ_DIO.xpr` project in the new `AFCZ_DIO` subdirectory.
You should open that project in the Vivado GUI and generate the bitstream.
After successful generation of the bitstream, you should export the hardware (using the "File/Export/Export\_Hardware" command in Vivado GUI menu) selecting `<local to project>` as destination and checking the `Include bitstream` option.

When export is finished, you may compile the Linux. To do that you should prepare the Petalinux environment
in the fresh console session (I have faced serious problems when I tried to reuse the session where Vviado was 
previously run), go to the `software` subdirectory of the cloned project and then run the `build.sh` script.
After preparing the initial configuration, the project will be compiled.

# The new method for building the design
This method attempts to automatically build the design in Vivado using the `compile.tcl` script. It has not been
thoroughly tested in different configurations, and therefore in case of any problems you should try the old method.
_This method works in the fresh cloned repository. If the design was already built and you want
to refresh it after modifications, you should do it manually. If the HW design was changed, compile in and export the hardware.
Then build the software as described in the "old" method._

To use the new method, you should adjust the paths to the Vivado and Petalinux installations in your system in the
`build.sh` script in the main directory of the cloned project.
Then you should simply run that script.

## Installation and testing
After successful build, the most important output are two files placed in the `software/images/linux` subdirectory:
1. BOOT.BIN
1. image.ub

You should copy them to the VFAT formatted SD card, and put it to the SD slot of the AFCZ board configured for bootoing from the SD card. It is assumed that two LVDS DIO FMC cards are connected to the FMC connectors, and that they are connected using the VHDCI cable.

When the system boots, you may login as root (the default password is also "root") and run the test programs:
1. clvdsgpio - written in C. It tests the transmission of data from FMC1 card to FMC2 card and in the opposite direction, including simultaneous transmission in one direction on lower 16 bits and in the opposite direction on the higher 16 bits. The application prints info about the individual errors and the total number of the errors.
1. test_lvdsgpio.py - written in Python. It tests transmission of data in idividual lines from FMC2 to FMC1, while the other lines transmit the data from FMC1 to FMC2. The application prints info about the progress of the test, displays the info about individual errors and the total number of the errors.

See [Wiki](https://gitlab.com/WZab/afcz_lvds_dio_2018.3/wikis/Description-of-components) for more details.
