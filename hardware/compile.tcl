set proj_name AFCZ_DIO
open_project ./${proj_name}/${proj_name}.xpr
update_compile_order -fileset sources_1
launch_runs impl_1 -to_step write_bitstream -jobs 16
wait_on_run impl_1
file mkdir ./${proj_name}/${proj_name}.sdk
file copy -force ./${proj_name}/${proj_name}.runs/impl_1/AFCZ_top_wrapper.sysdef ./${proj_name}/${proj_name}.sdk/AFCZ_top_wrapper.hdf

