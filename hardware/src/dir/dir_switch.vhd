-------------------------------------------------------------------------------
-- Title      : FMC_LVDS direction controller
-- Project    : 
-------------------------------------------------------------------------------
-- File       : dir_switch.vhd
-- Author     : Wojciech M. Zabolotny <wzab@ise.pw.edu.pl>
-- Company    : Institute of Electronic Systems
-- License    : BSD
-- Created    : 2015-05-14
-- Last update: 2019-07-16
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This block allows you to set the directions in the FMC_LVDS board
-------------------------------------------------------------------------------
-- Copyright (c) 2015 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2015-05-14  1.0      WZab    Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dir_switch is

  generic (
    CHAIN_LEN : integer := 32           -- Length of the bit stream
    );

  port (
    -- system interface
    clk   : in  std_logic;
    rst_p : in  std_logic;
    -- Input with desired directions
    dirs  : in  std_logic_vector(CHAIN_LEN-1 downto 0);
    -- Output signals for the SPI-like chain
    ser   : out std_logic := '0';
    srclk : out std_logic := '0';
    rclk  : out std_logic := '0');

end entity dir_switch;

architecture beh of dir_switch is

  signal count : integer range -1 to CHAIN_LEN-1 := CHAIN_LEN-1;
  signal step  : integer range 0 to 1            := 0;

  -- purpose: Because the FMC_LVDS card has enable lines swapped
  -- in pairs, we need a function that toggles the LSB in an integer...
  function int_xor_1 (
    constant val : integer)
    return integer is
    variable res  : integer;
    variable ires : signed(31 downto 0);
  begin  -- function int_xor_1
    ires    := to_signed(val, 32);
    ires(0) := not ires(0);
    res     := to_integer(ires);
    return res;
  end function int_xor_1;

begin  -- architecture beh

  -- Signal rclk must be generated in oposite phase!
  p1 : process (clk) is
  begin  -- process p1
    if clk'event and clk = '1' then     -- rising clock edge
      if rst_p = '1' then               -- asynchronous reset (active low)
        count <= CHAIN_LEN-1;
        step  <= 0;
      else
        if count >= 0 then
          if step = 0 then
            ser   <= dirs(int_xor_1(count));
            rclk  <= '0';
            srclk <= '0';
            step  <= 1;
          else
            srclk <= '1';
            count <= count - 1;
            step  <= 0;
          end if;
        else
          if step = 0 then
            ser   <= '0';
            rclk  <= '0';
            srclk <= '0';
            step  <= 1;
          else
            rclk  <= '1';
            step  <= 0;
            count <= CHAIN_LEN-1;
          end if;
        end if;
      end if;

    end if;
  end process p1;


end architecture beh;
