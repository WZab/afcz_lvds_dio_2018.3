set_property PACKAGE_PIN B25 [get_ports FMC1_LATCH]
set_property IOSTANDARD LVCMOS18 [get_ports FMC1_LATCH]
set_property PACKAGE_PIN B26 [get_ports FMC1_SCLK]
set_property IOSTANDARD LVCMOS18 [get_ports FMC1_SCLK]
set_property PACKAGE_PIN A22 [get_ports FMC1_SIN]
set_property IOSTANDARD LVCMOS18 [get_ports FMC1_SIN]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[0]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[10]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[10]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[11]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[11]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[12]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[12]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[13]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[13]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[14]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[14]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[15]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[15]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[16]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[16]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[17]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[17]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[18]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[18]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[19]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[19]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[1]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[1]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[20]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[20]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[21]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[21]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[22]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[22]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[23]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[23]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[24]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[24]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[25]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[25]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[26]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[26]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[27]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[27]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[28]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[28]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[29]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[29]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[2]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[2]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[30]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[30]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[31]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[31]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[3]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[3]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[4]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[4]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[5]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[5]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[6]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[6]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[7]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[7]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[8]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[8]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_N[9]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_N[9]}]
set_property PACKAGE_PIN E27 [get_ports {FMC1_SIO_P[0]}]
set_property PACKAGE_PIN E28 [get_ports {FMC1_SIO_N[0]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[0]}]
set_property PACKAGE_PIN H26 [get_ports {FMC1_SIO_P[10]}]
set_property PACKAGE_PIN H27 [get_ports {FMC1_SIO_N[10]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[10]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[10]}]
set_property PACKAGE_PIN G30 [get_ports {FMC1_SIO_P[11]}]
set_property PACKAGE_PIN G31 [get_ports {FMC1_SIO_N[11]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[11]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[11]}]
set_property PACKAGE_PIN H28 [get_ports {FMC1_SIO_P[12]}]
set_property PACKAGE_PIN H29 [get_ports {FMC1_SIO_N[12]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[12]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[12]}]
set_property PACKAGE_PIN D26 [get_ports {FMC1_SIO_P[13]}]
set_property PACKAGE_PIN D27 [get_ports {FMC1_SIO_N[13]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[13]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[13]}]
set_property PACKAGE_PIN D29 [get_ports {FMC1_SIO_P[14]}]
set_property PACKAGE_PIN D30 [get_ports {FMC1_SIO_N[14]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[14]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[14]}]
set_property PACKAGE_PIN E29 [get_ports {FMC1_SIO_P[15]}]
set_property PACKAGE_PIN E30 [get_ports {FMC1_SIO_N[15]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[15]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[15]}]
set_property PACKAGE_PIN F30 [get_ports {FMC1_SIO_P[16]}]
set_property PACKAGE_PIN F31 [get_ports {FMC1_SIO_N[16]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[16]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[16]}]
set_property PACKAGE_PIN F27 [get_ports {FMC1_SIO_P[17]}]
set_property PACKAGE_PIN F28 [get_ports {FMC1_SIO_N[17]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[17]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[17]}]
set_property PACKAGE_PIN F21 [get_ports {FMC1_SIO_P[18]}]
set_property PACKAGE_PIN F22 [get_ports {FMC1_SIO_N[18]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[18]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[18]}]
set_property PACKAGE_PIN C27 [get_ports {FMC1_SIO_P[19]}]
set_property PACKAGE_PIN B28 [get_ports {FMC1_SIO_N[19]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[19]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[19]}]
set_property PACKAGE_PIN G28 [get_ports {FMC1_SIO_P[1]}]
set_property PACKAGE_PIN G29 [get_ports {FMC1_SIO_N[1]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[1]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[1]}]
set_property PACKAGE_PIN B29 [get_ports {FMC1_SIO_P[20]}]
set_property PACKAGE_PIN B30 [get_ports {FMC1_SIO_N[20]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[20]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[20]}]
set_property PACKAGE_PIN C28 [get_ports {FMC1_SIO_P[21]}]
set_property PACKAGE_PIN C29 [get_ports {FMC1_SIO_N[21]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[21]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[21]}]
set_property PACKAGE_PIN A30 [get_ports {FMC1_SIO_P[22]}]
set_property PACKAGE_PIN A31 [get_ports {FMC1_SIO_N[22]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[22]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[22]}]
set_property PACKAGE_PIN A27 [get_ports {FMC1_SIO_P[23]}]
set_property PACKAGE_PIN A28 [get_ports {FMC1_SIO_N[23]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[23]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[23]}]
set_property PACKAGE_PIN C31 [get_ports {FMC1_SIO_P[24]}]
set_property PACKAGE_PIN B31 [get_ports {FMC1_SIO_N[24]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[24]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[24]}]
set_property PACKAGE_PIN D21 [get_ports {FMC1_SIO_P[25]}]
set_property PACKAGE_PIN D22 [get_ports {FMC1_SIO_N[25]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[25]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[25]}]
set_property PACKAGE_PIN F25 [get_ports {FMC1_SIO_P[26]}]
set_property PACKAGE_PIN E25 [get_ports {FMC1_SIO_N[26]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[26]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[26]}]
set_property PACKAGE_PIN E24 [get_ports {FMC1_SIO_P[27]}]
set_property PACKAGE_PIN D25 [get_ports {FMC1_SIO_N[27]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[27]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[27]}]
set_property PACKAGE_PIN B23 [get_ports {FMC1_SIO_P[28]}]
set_property PACKAGE_PIN B24 [get_ports {FMC1_SIO_N[28]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[28]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[28]}]
set_property PACKAGE_PIN C21 [get_ports {FMC1_SIO_P[29]}]
set_property PACKAGE_PIN C22 [get_ports {FMC1_SIO_N[29]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[29]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[29]}]
set_property PACKAGE_PIN M25 [get_ports {FMC1_SIO_P[2]}]
set_property PACKAGE_PIN L25 [get_ports {FMC1_SIO_N[2]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[2]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[2]}]
set_property PACKAGE_PIN A25 [get_ports {FMC1_SIO_P[30]}]
set_property PACKAGE_PIN A26 [get_ports {FMC1_SIO_N[30]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[30]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[30]}]
set_property PACKAGE_PIN B21 [get_ports {FMC1_SIO_P[31]}]
set_property PACKAGE_PIN A21 [get_ports {FMC1_SIO_N[31]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[31]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[31]}]
set_property PACKAGE_PIN L27 [get_ports {FMC1_SIO_P[3]}]
set_property PACKAGE_PIN L28 [get_ports {FMC1_SIO_N[3]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[3]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[3]}]
set_property PACKAGE_PIN M26 [get_ports {FMC1_SIO_P[4]}]
set_property PACKAGE_PIN L26 [get_ports {FMC1_SIO_N[4]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[4]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[4]}]
set_property PACKAGE_PIN K28 [get_ports {FMC1_SIO_P[5]}]
set_property PACKAGE_PIN J29 [get_ports {FMC1_SIO_N[5]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[5]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[5]}]
set_property PACKAGE_PIN K25 [get_ports {FMC1_SIO_P[6]}]
set_property PACKAGE_PIN J25 [get_ports {FMC1_SIO_N[6]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[6]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[6]}]
set_property PACKAGE_PIN K29 [get_ports {FMC1_SIO_P[7]}]
set_property PACKAGE_PIN K30 [get_ports {FMC1_SIO_N[7]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[7]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[7]}]
set_property PACKAGE_PIN J26 [get_ports {FMC1_SIO_P[8]}]
set_property PACKAGE_PIN J27 [get_ports {FMC1_SIO_N[8]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[8]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[8]}]
set_property PACKAGE_PIN G26 [get_ports {FMC1_SIO_P[9]}]
set_property PACKAGE_PIN F26 [get_ports {FMC1_SIO_N[9]}]
set_property IOSTANDARD LVDS [get_ports {FMC1_SIO_P[9]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC1_SIO_P[9]}]
set_property PACKAGE_PIN N19 [get_ports FMC2_LATCH]
set_property IOSTANDARD LVCMOS18 [get_ports FMC2_LATCH]
set_property PACKAGE_PIN N18 [get_ports FMC2_SCLK]
set_property IOSTANDARD LVCMOS18 [get_ports FMC2_SCLK]
set_property PACKAGE_PIN N14 [get_ports FMC2_SIN]
set_property IOSTANDARD LVCMOS18 [get_ports FMC2_SIN]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[0]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[10]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[10]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[11]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[11]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[12]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[12]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[13]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[13]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[14]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[14]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[15]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[15]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[16]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[16]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[17]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[17]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[18]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[18]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[19]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[19]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[1]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[1]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[20]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[20]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[21]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[21]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[22]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[22]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[23]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[23]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[24]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[24]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[25]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[25]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[26]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[26]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[27]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[27]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[28]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[28]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[29]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[29]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[2]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[2]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[30]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[30]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[31]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[31]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[3]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[3]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[4]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[4]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[5]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[5]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[6]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[6]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[7]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[7]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[8]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[8]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_N[9]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_N[9]}]
set_property PACKAGE_PIN G24 [get_ports {FMC2_SIO_P[0]}]
set_property PACKAGE_PIN G25 [get_ports {FMC2_SIO_N[0]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[0]}]
set_property PACKAGE_PIN J24 [get_ports {FMC2_SIO_P[10]}]
set_property PACKAGE_PIN H24 [get_ports {FMC2_SIO_N[10]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[10]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[10]}]
set_property PACKAGE_PIN B20 [get_ports {FMC2_SIO_P[11]}]
set_property PACKAGE_PIN A20 [get_ports {FMC2_SIO_N[11]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[11]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[11]}]
set_property PACKAGE_PIN B16 [get_ports {FMC2_SIO_P[12]}]
set_property PACKAGE_PIN A16 [get_ports {FMC2_SIO_N[12]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[12]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[12]}]
set_property PACKAGE_PIN A18 [get_ports {FMC2_SIO_P[13]}]
set_property PACKAGE_PIN A17 [get_ports {FMC2_SIO_N[13]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[13]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[13]}]
set_property PACKAGE_PIN C17 [get_ports {FMC2_SIO_P[14]}]
set_property PACKAGE_PIN C16 [get_ports {FMC2_SIO_N[14]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[14]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[14]}]
set_property PACKAGE_PIN B19 [get_ports {FMC2_SIO_P[15]}]
set_property PACKAGE_PIN B18 [get_ports {FMC2_SIO_N[15]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[15]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[15]}]
set_property PACKAGE_PIN C19 [get_ports {FMC2_SIO_P[16]}]
set_property PACKAGE_PIN C18 [get_ports {FMC2_SIO_N[16]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[16]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[16]}]
set_property PACKAGE_PIN H17 [get_ports {FMC2_SIO_P[17]}]
set_property PACKAGE_PIN G16 [get_ports {FMC2_SIO_N[17]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[17]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[17]}]
set_property PACKAGE_PIN H19 [get_ports {FMC2_SIO_P[18]}]
set_property PACKAGE_PIN H18 [get_ports {FMC2_SIO_N[18]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[18]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[18]}]
set_property PACKAGE_PIN F20 [get_ports {FMC2_SIO_P[19]}]
set_property PACKAGE_PIN E20 [get_ports {FMC2_SIO_N[19]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[19]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[19]}]
set_property PACKAGE_PIN F17 [get_ports {FMC2_SIO_P[1]}]
set_property PACKAGE_PIN F16 [get_ports {FMC2_SIO_N[1]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[1]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[1]}]
set_property PACKAGE_PIN D17 [get_ports {FMC2_SIO_P[20]}]
set_property PACKAGE_PIN D16 [get_ports {FMC2_SIO_N[20]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[20]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[20]}]
set_property PACKAGE_PIN E19 [get_ports {FMC2_SIO_P[21]}]
set_property PACKAGE_PIN E18 [get_ports {FMC2_SIO_N[21]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[21]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[21]}]
set_property PACKAGE_PIN F18 [get_ports {FMC2_SIO_P[22]}]
set_property PACKAGE_PIN E17 [get_ports {FMC2_SIO_N[22]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[22]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[22]}]
set_property PACKAGE_PIN G19 [get_ports {FMC2_SIO_P[23]}]
set_property PACKAGE_PIN G18 [get_ports {FMC2_SIO_N[23]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[23]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[23]}]
set_property PACKAGE_PIN K19 [get_ports {FMC2_SIO_P[24]}]
set_property PACKAGE_PIN J19 [get_ports {FMC2_SIO_N[24]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[24]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[24]}]
set_property PACKAGE_PIN J16 [get_ports {FMC2_SIO_P[25]}]
set_property PACKAGE_PIN H16 [get_ports {FMC2_SIO_N[25]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[25]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[25]}]
set_property PACKAGE_PIN K17 [get_ports {FMC2_SIO_P[26]}]
set_property PACKAGE_PIN J17 [get_ports {FMC2_SIO_N[26]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[26]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[26]}]
set_property PACKAGE_PIN K15 [get_ports {FMC2_SIO_P[27]}]
set_property PACKAGE_PIN J15 [get_ports {FMC2_SIO_N[27]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[27]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[27]}]
set_property PACKAGE_PIN M18 [get_ports {FMC2_SIO_P[28]}]
set_property PACKAGE_PIN L18 [get_ports {FMC2_SIO_N[28]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[28]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[28]}]
set_property PACKAGE_PIN M15 [get_ports {FMC2_SIO_P[29]}]
set_property PACKAGE_PIN L15 [get_ports {FMC2_SIO_N[29]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[29]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[29]}]
set_property PACKAGE_PIN N22 [get_ports {FMC2_SIO_P[2]}]
set_property PACKAGE_PIN N23 [get_ports {FMC2_SIO_N[2]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[2]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[2]}]
set_property PACKAGE_PIN L17 [get_ports {FMC2_SIO_P[30]}]
set_property PACKAGE_PIN L16 [get_ports {FMC2_SIO_N[30]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[30]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[30]}]
set_property PACKAGE_PIN N16 [get_ports {FMC2_SIO_P[31]}]
set_property PACKAGE_PIN M16 [get_ports {FMC2_SIO_N[31]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[31]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[31]}]
set_property PACKAGE_PIN L20 [get_ports {FMC2_SIO_P[3]}]
set_property PACKAGE_PIN K20 [get_ports {FMC2_SIO_N[3]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[3]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[3]}]
set_property PACKAGE_PIN L22 [get_ports {FMC2_SIO_P[4]}]
set_property PACKAGE_PIN L23 [get_ports {FMC2_SIO_N[4]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[4]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[4]}]
set_property PACKAGE_PIN M20 [get_ports {FMC2_SIO_P[5]}]
set_property PACKAGE_PIN L21 [get_ports {FMC2_SIO_N[5]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[5]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[5]}]
set_property PACKAGE_PIN K23 [get_ports {FMC2_SIO_P[6]}]
set_property PACKAGE_PIN K24 [get_ports {FMC2_SIO_N[6]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[6]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[6]}]
set_property PACKAGE_PIN N21 [get_ports {FMC2_SIO_P[7]}]
set_property PACKAGE_PIN M21 [get_ports {FMC2_SIO_N[7]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[7]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[7]}]
set_property PACKAGE_PIN J22 [get_ports {FMC2_SIO_P[8]}]
set_property PACKAGE_PIN H22 [get_ports {FMC2_SIO_N[8]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[8]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[8]}]
set_property PACKAGE_PIN J21 [get_ports {FMC2_SIO_P[9]}]
set_property PACKAGE_PIN H21 [get_ports {FMC2_SIO_N[9]}]
set_property IOSTANDARD LVDS [get_ports {FMC2_SIO_P[9]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {FMC2_SIO_P[9]}]


set_input_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports {FMC*_SIO_*[*]}]
set_input_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports {FMC*_SIO_*[*]}]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports {FMC*_SIO_*[*]}]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports {FMC*_SIO_*[*]}]
set_false_path -from [get_clocks clk_pl_0] -through [get_nets -hierarchical -regexp .*IOBUFDS_I/T.*] -to [get_ports -regexp .*FMC.*SIO.*]

set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC1_LATCH]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC1_LATCH]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC1_SCLK]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC1_SCLK]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC1_SIN]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC1_SIN]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC2_LATCH]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC2_LATCH]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC2_SCLK]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC2_SCLK]
set_output_delay -clock [get_clocks clk_pl_0] -min -add_delay 0.000 [get_ports FMC2_SIN]
set_output_delay -clock [get_clocks clk_pl_0] -max -add_delay 4.000 [get_ports FMC2_SIN]
