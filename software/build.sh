#!/bin/bash
set -e
mkdir -p project-spec/hw-description
petalinux-config --get-hw-description ../hardware/AFCZ_DIO/AFCZ_DIO.sdk --oldconfig
petalinux-build -x mrproper ; petalinux-build
petalinux-package --boot --fsbl images/linux/zynqmp_fsbl.elf --pmufw --fpga images/linux/system.bit --u-boot --force --boot-device sd

