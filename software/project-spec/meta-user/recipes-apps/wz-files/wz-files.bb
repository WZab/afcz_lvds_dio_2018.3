#
# This file is the wz-scripts recipe.
#

SUMMARY = "Simple wz-scripts application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

RDEPENDS_${PN} += "bash"
FILES_${PN} += "/etc/*"
FILES_${PN} += "/usr/bin/*"
SRC_URI = "file://etc/modprobe.d/* \
        file://usr/bin/* \
	"

S = "${WORKDIR}"

do_install() {
             install -d ${D}/etc/modprobe.d
             install -m 0755 ${S}/etc/modprobe.d/* ${D}/etc/modprobe.d
             install -d ${D}/usr/bin
             install -m 0755 ${S}/usr/bin/* ${D}/usr/bin
}
