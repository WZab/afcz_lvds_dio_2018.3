#
# This file is the pylvdsgpio recipe.
#

SUMMARY = "Simple application for testing of LVDS GPIO written in Python"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://lvdsgpio.py \
	   file://liblvdsgpio.c \
	   file://test_lvdsgpio.py \
	   file://Makefile \
		  "

S = "${WORKDIR}"
DEPENDS = " python3"
RDEPENDS_${PN}_append += " \
 python3 \
 python3-core \
"

# Section below not needed for Python app
do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -d ${D}${libdir}
	     install -m 0755 liblvdsgpio.so.1 ${D}${libdir}
	     install -m 0755 lvdsgpio.py ${D}${bindir}
	     install -m 0755 test_lvdsgpio.py ${D}${bindir}
}
