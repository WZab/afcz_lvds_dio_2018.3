/*
 * Library for using 2 32 LVDS Digital I/O FMC cards
 * ( https://www.ohwr.org/project/fmc-dio-32chlvdsa )
 * from Python
 * Written by Wojciech M. Zabołotny (wzab01@gmail.com)
 * Available under BSD license
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define GPIO_MMAP_LENGTH 0x1000
// The addresses of the AXI GPIOs controlling both boards
// (you should get them from the device tree of from the Address 
// Editor in Vivado)
// both should be page aligned!
static const unsigned long lgpio1 = 0xb0000000;
static const unsigned long lgpio2 = 0xb0001000;

//Structure describing the registers of AXI GPIO
//(see https://www.xilinx.com/support/documentation/ip_documentation/axi_gpio/v2_0/pg144-axi-gpio.pdf , Table 2-4 )
typedef struct __attribute__ ((aligned (4)))
{
    volatile uint32_t gpio_data;
    volatile uint32_t gpio_tri;
    volatile uint32_t gpio2_data;
    volatile uint32_t gpio2_tri;
    volatile uint32_t pad0[(0x011c-0x000c-4)/4]; //Fill the gap between groups of registers
    volatile uint32_t gier;
    volatile uint32_t ip_ier;
    volatile uint32_t ip_isr;
}
axi_gpio;

static axi_gpio * sgpio1 = NULL;
static axi_gpio * sgpio2 = NULL;

static int is_open = 0; //Flag preventing multiple opens
int fd = -1; //File descriptor for /dev/mem

/* 1 - line is input, 0 - line is output */
static void set_dir(axi_gpio * gp, uint32_t dir)
{
    gp->gpio2_tri = 0x0; /* always output */
    //Here we need to reverse bits in gpio2_data
    gp->gpio2_data = dir;
    gp->gpio_tri = dir;
    __sync_synchronize();
}

void set_dir_gpio1(uint32_t dir)
{
    set_dir(sgpio1, dir);
}

void set_dir_gpio2(uint32_t dir)
{
    set_dir(sgpio2, dir);
}


static void set_val(axi_gpio * gp, uint32_t val)
{
    gp->gpio_data = val;
    __sync_synchronize();
}

void set_val_gpio1(uint32_t val)
{
    set_val(sgpio1, val);
}

void set_val_gpio2(uint32_t val)
{
    set_val(sgpio2, val);
}


static inline uint32_t get_val(axi_gpio *gp)
{
    __sync_synchronize();
    return gp->gpio_data;
}

uint32_t get_val_gpio1()
{
    return get_val(sgpio1);
}

uint32_t get_val_gpio2()
{
    return get_val(sgpio2);
}


void close_gpios()
{
    if(sgpio1 && (sgpio1 != MAP_FAILED)) {
        munmap(sgpio1,GPIO_MMAP_LENGTH);
    }
    sgpio1 = NULL;
    if(sgpio2 && (sgpio2 != MAP_FAILED)) {
        munmap(sgpio2,GPIO_MMAP_LENGTH);
    }
    sgpio2 = NULL;
    if(fd != -1) {
        close(fd);
        fd = -1;
    }
    is_open = 0;
}

int32_t open_gpios()
{
    int res;
    if(is_open) {
        fprintf(stderr,"Already opened!\n");
        return -1;
    }
    /* open the /dev/mem */
    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) {
        perror("Can't open /dev/mem");
        return -2;
    };
    /* Map both devices */
    sgpio1 = (axi_gpio *) mmap(0, GPIO_MMAP_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (long int) lgpio1);
    if(sgpio1 == MAP_FAILED) {
        perror("Can't map first GPIO\n");
        res = -3;
        goto open_cleanup;
    }
    sgpio2 = (axi_gpio *) mmap(0, GPIO_MMAP_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (long int) lgpio2);
    if(sgpio2 == MAP_FAILED) {
        perror("Can't map second GPIO\n");
        res = -4;
        goto open_cleanup;
    }
    is_open = 1;
    return 0;
open_cleanup:
    close_gpios();
    return res;
}
