#!/usr/bin/python3
""" Example Python application for testing FMC LVDS boards 
    https://www.ohwr.org/project/fmc-dio-32chlvdsa
    Written by Wojciech M. Zabolotny (wzab01@gmail.com)
    Published under BSD license.
    That application sets single line for transmission FMC2->FMC1, while all the others
    are sending in the opposite direction (FMC1->FMC2). That allows us to check if the enable lines
    are not swapped (in fact they are, and it had to be worked around  in the FPGA firmware)
"""
noftests = 1000
import time
import random
total_errors1 = 0
total_errors2 = 0
import lvdsgpio as lv
with lv.LvdsGpio() as gp:
    for bitnr in range(0,32):
        nof_errors1 = 0
        nof_errors2 = 0
        mask1 = 1 << bitnr
        mask2 = 0xffffffff - mask1
        gp.dir1 = mask1
        gp.dir2 = mask2
        time.sleep(1e-6)
        print("Test for bit #"+str(bitnr))
        for testnr in range(0,noftests):
            val1 = random.randint(0,0xffffffff)
            val2 = random.randint(0,0xffffffff)
            gp.val1 = val1
            gp.val2 = val2
            time.sleep(1e-6)
            # Check transmission 1 -> 2
            diff1 = (val1 ^ gp.val2) &  mask2
            if (diff1 != 0):
                print("Error, transmitted: {:08x} received: {:08x}".format(val1 & mask2,gp.val2 & mask2))
                nof_errors1 += 1
                total_errors1 += 1
            # Check transmission 2 -> 1
            diff2 = (val2 ^ gp.val1) &  mask1
            if (diff2 != 0):
                print("Error, transmitted: {:08x} received: {:08x}".format(val2 & mask1 ,gp.val1 & mask1))
                nof_errors2 += 1
                total_errors2 += 1
        print("#errors 1->2: {:d}, #errors 2->1: {:d}".format(nof_errors1,nof_errors2))
    print("Summary, for all lines:")
    print("#errors 1->2: {:d}, #errors 2->1: {:d}".format(nof_errors1,nof_errors2))
        
