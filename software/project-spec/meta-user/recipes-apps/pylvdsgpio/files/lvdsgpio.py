"""
Module lvdsgpio providing the python interface for
32 LVDS Digital I/O FMC cards
https://www.ohwr.org/project/fmc-dio-32chlvdsa
Written by Wojciech M. Zabolotny (wzab01@gmail.com)
Available under BSD license
"""
import ctypes
LGPLIB = ctypes.cdll.LoadLibrary("liblvdsgpio.so.1")
#Set the right return type for the get_val_gpio functions
LGPLIB.get_val_gpio1.restype=ctypes.c_uint32
LGPLIB.get_val_gpio2.restype=ctypes.c_uint32

class LvdsGpioException(Exception):
    """ Minimalistic exception class
    """
    pass

class LvdsGpio(object):
    """ Class for accessing both FMC cards
        It allows you to modify the direction by simple 
        writing to the "dir1" or "dir2" attribute of the object
        Similarly the data may be written or read by simple
        access to the "val1" or "val2" attribute
    """
    def __init__(self):
        """ Constructor of the LvdsGpio object.
            Please note that direction is unknown until set!
        """
        self.is_open = False
        self.shadow_dir1 = None
        self.shadow_dir2 = None

    def xopen(self):
        if self.is_open:
            raise LvdsGpioException("Object already opened!")
        res = LGPLIB.open_gpios()
        if res < 0:
            raise LvdsGpioException("open_gpios returned: "+str(res))
        self.is_open = True

    def __enter__(self):
        self.xopen()
        return self

    def xclose(self):
        LGPLIB.close_gpios()
        self.is_open = False

    def __exit__(self, atype, value, traceback):
        self.xclose()

    @property
    def dir1(self):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        return self.shadow_dir1

    @dir1.setter
    def dir1(self, val):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        LGPLIB.set_dir_gpio1(val)
        self.shadow_dir1 = val

    @property
    def dir2(self):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        return self.shadow_dir2

    @dir2.setter
    def dir2(self, val):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        LGPLIB.set_dir_gpio2(val)
        self.shadow_dir2 = val

    @property
    def val1(self):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        return LGPLIB.get_val_gpio1()

    @val1.setter
    def val1(self, val):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        LGPLIB.set_val_gpio1(val)

    @property
    def val2(self):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        return LGPLIB.get_val_gpio2()

    @val2.setter
    def val2(self, val):
        if not self.is_open:
            raise LvdsGpioException("Object is not open!")
        LGPLIB.set_val_gpio2(val)
