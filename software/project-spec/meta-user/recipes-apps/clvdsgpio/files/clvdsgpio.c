/*
 * Test application for 32 LVDS Digital I/O FMC cards
 * https://www.ohwr.org/project/fmc-dio-32chlvdsa
 * Written by Wojciech M. Zabołotny (wzab01@gmail.com)
 * Available under BSD license
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// The addresses of the AXI GPIOs controlling both boards
// (you should get them from the device tree of from the 
// Address Editor in Vviado)
// both should be page aligned!
const unsigned long lgpio1 = 0xb0000000;
const unsigned long lgpio2 = 0xb0001000;
#define GPIO_MMAP_LENGTH 0x1000

//Structures describing the registers of AXI GPIO
//(see https://www.xilinx.com/support/documentation/ip_documentation/axi_gpio/v2_0/pg144-axi-gpio.pdf , Table 2-4 )


typedef struct __attribute__ ((aligned (4)))
{
    volatile uint32_t gpio_data;
    volatile uint32_t gpio_tri;
    volatile uint32_t gpio2_data;
    volatile uint32_t gpio2_tri;
    volatile uint32_t pad0[(0x011c-0x000c-4)/4]; //Fill the gap between groups of registers
    volatile uint32_t gier;
    volatile uint32_t ip_ier;
    volatile uint32_t ip_isr;
}
axi_gpio;

axi_gpio * sgpio1 = NULL;
axi_gpio * sgpio2 = NULL;
int fd = -1;

/* 1 - line is input, 0 - line is output */
void set_dir(axi_gpio * gp, uint32_t dir)
{
    gp->gpio2_tri = 0x0; /* always output */
    //Here we need to reverse bits in gpio2_data
    uint32_t br=0;
    gp->gpio2_data = dir;
    gp->gpio_tri = dir;
    __sync_synchronize();
}
void set_val(axi_gpio * gp, uint32_t val)
{
    gp->gpio_data = val;
    __sync_synchronize();
}

uint32_t get_val(axi_gpio *gp)
{
    __sync_synchronize();
    return gp->gpio_data;
}

int main(int argc, char **argv)
{
    int i;
    int res = 0;
    int nof_errors1 = 0;
    int nof_errors2 = 0;
    int nof_errors3 = 0;
    int nof_errors4 = 0;
    /* open the /dev/mem */
    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) {
        perror("Can't open /dev/mem");
        res = 1;
        goto main_cleanup;
    };
    printf("/dev/mem opened.\n");
    fflush(stdout);
    /* Map both devices */
    sgpio1 = (axi_gpio *) mmap(0, GPIO_MMAP_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, lgpio1);
    if(sgpio1 == MAP_FAILED) {
        perror("Can't map first GPIO\n");
        res = 2;
        goto main_cleanup;
    }
    axi_gpio * sgpio2 = (axi_gpio *) mmap(0, GPIO_MMAP_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, lgpio2);
    if(sgpio2 == MAP_FAILED) {
        perror("Can't map second GPIO\n");
        res = 3;
        goto main_cleanup;
    }
    /* Now we can access the gpios */
    /* First we test transmission from the first board to the second */
    printf("Testing transmission FMC1->FMC2\n");
    set_dir(sgpio1,0x0);
    set_dir(sgpio2,0xffffffff);
    /* wait until direction is switched */
    usleep(10);
    for(i=0; i<1000; i++) {
        uint32_t test_val = random();
        set_val(sgpio1,test_val);
        usleep(1); /* you may vary that value */
        uint32_t read_val = get_val(sgpio2);
        if(read_val != test_val) {
            nof_errors1++;
            printf("1->2 sent: %8.8x received: %8.8x\n",test_val,read_val);
        }
    }
    /* now we test transmission from the 2nd to the 1st board */
    printf("Testing transmission FMC2->FMC1\n");
    set_dir(sgpio2,0x0);
    set_dir(sgpio1,0xffffffff);
    /* wait until direction is switched */
    usleep(10);
    for(i=0; i<1000; i++) {
        uint32_t test_val = random();
        set_val(sgpio2,test_val);
        usleep(1); /* you may vary that value */
        uint32_t read_val = get_val(sgpio1);
        if(read_val != test_val) {
            nof_errors2++;
            printf("2->1  sent: %8.8x received: %8.8x\n",test_val,read_val);
        }
    }
    /* now we test transmission 1->2 in the lower 16 bits and 2->1 in the upper 16 bits */
    printf("Testing transmission FMC1->FMC2 on lower bits and FMC2->FMC1 on upper bits\n");
    set_dir(sgpio2,0x0000ffff);
    set_dir(sgpio1,0xffff0000);
    /* wait until direction is switched */
    usleep(10);
    for(i=0; i<1000; i++) {
        uint32_t test_val1 = random();
        uint32_t test_val2 = random();
        set_val(sgpio1,test_val1);
        set_val(sgpio2,test_val2);
        usleep(1); /* you may vary that value */
        uint32_t read_val1 = get_val(sgpio1);
        uint32_t read_val2 = get_val(sgpio2);
        if((read_val2 & 0x0000ffff) != (test_val1 & 0x0000ffff)) {
            nof_errors3++;
            printf("L16 1->2   sent: %8.8x received: %8.8x\n",(test_val1 & 0xffff),(read_val2& 0xffff));
        }
        if((read_val1 & 0xffff0000) != (test_val2 & 0xffff0000)) {
            nof_errors4++;
            printf("H16 2->1  sent: %8.8x received: %8.8x\n",(test_val2 & 0xffff0000),(read_val1 & 0xffff0000));
        }
    }
    printf("Summary\n 1->2: %d errors\n 2->1: %d errors\n L16 1->2: %d errors\n H16 2->1: %d,errors\n",
           nof_errors1, nof_errors2, nof_errors3, nof_errors4);
main_cleanup:
    if(sgpio1 && (sgpio1 != MAP_FAILED)) {
        munmap(sgpio1,GPIO_MMAP_LENGTH);
    }
    sgpio1 = NULL;
    if(sgpio2 && (sgpio2 != MAP_FAILED)) {
        munmap(sgpio2,GPIO_MMAP_LENGTH);
    }
    sgpio2 = NULL;
    if(fd != -1) {
        close(fd);
        fd = -1;
    }
    return res;
}
